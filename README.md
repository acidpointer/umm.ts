# Universal Mod Manager

This is universal mod manager!

## Features
1. Writtend in pure TypeScript
2. Use Deno as runtime. (Much better than NodeJS, seriously)
3. Cool dependency injection from scratch (inspired by NestJS)
4. Merging by using symlinks and if needed - copying
5. Fancy and easy to read definitions format

## Goals
1. Easy CLI
2. File based project configuration (definitions.toml)
3. Integrity check
4. Use as little 3-d party dependencies as possible


## WARNING !!!
This project is in early progress and super experimental. Use it it you really know what you do!