/**
 * HashFile
 * Fast file hasher for Deno!
 * By default use XXHash128
 * 
 * Other hashers from 'hash-wasm' can be provided through constructor
 * 
 * 
 * @author acidpointer and others
 * @version 1.0
 */


// deno-lint-ignore-file no-inner-declarations no-var
import { createXXHash128 } from "hash-wasm";

// Source: https://stackoverflow.com/a/63287199

const chunkSize = 64 * 1024 * 1024;

export class HashFile {
  private fileReader: FileReader = new FileReader();

  // Ahh, typings sooooo broken
  // Should be IHasher from WASMInterface
  // deno-lint-ignore no-explicit-any
  private hasher: any = null;

  // Let's use fastest hash algorithm by default
  constructor(private hashFn: (seed?: number) => Promise<unknown> = createXXHash128) {}


  async hash(filePath: string) {
    const fileRaw = await Deno.readFile(filePath);
    const file = new Blob([fileRaw]);

    if (!this.hasher) {
      this.hasher = await this.hashFn();
    } else {
      this.hasher.init();
    }

    const chunkNumber = Math.floor(file.size / chunkSize);

    for (var i = 0; i <= chunkNumber; i++) {
      const chunk = file.slice(
        chunkSize * i,
        Math.min(chunkSize * (i + 1), file.size),
      );
      await this.hashChunk(chunk);
    }

    const hash = this.hasher.digest();
    return Promise.resolve(hash);
  }

  private async hashChunk(chunk: Blob): Promise<void> {
    return new Promise((resolve, reject) => {
      this.fileReader.onload = async (e) => {
        const res = e.target?.result;

        if (!res) {
          reject('Failed to read chunk');
        }

        const view = new Uint8Array(res as ArrayBuffer);
        this.hasher?.update(view);
        resolve();
      };

      this.fileReader.readAsArrayBuffer(chunk);
    });
  }
}
