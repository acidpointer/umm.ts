/**
 * Stupid Simple Logger aka SSL :D
 *
 * Welcome to the simple and stupid logger!
 * It's so fun to re-invent wheels, but, this logger may be unfinished for a while.
 * But really, many many of us need stupid and simple logger, is'nt it?
 *
 * @author acidpointer
 * @version 1.0
 */

import { colors } from "cliffy_ansi";

export const consoleLogHandler = (
  level: LogLevel,
  context: string,
  message: string,
  ...args: unknown[]
) => {
  const date = new Date();

  const pid = Deno.pid;
  const seconds = date.getSeconds();
  const minutes = date.getMinutes();
  const hours = date.getHours();
  const millis = date.getMilliseconds();
  const timeStr = `${hours}:${minutes}:${seconds}.${millis}`;
  const appName = state.appName ? `[${state.appName}]` : "";

  const levelFmtData: { str: string; color: (str: string) => string } = {
    str: "UNKNOWN",
    color: colors.bold.white,
  };

  switch (level) {
    case LogLevel.INFO: {
      levelFmtData.color = colors.bold.green;
      levelFmtData.str = "INFO";
      break;
    }
    case LogLevel.WARN: {
      levelFmtData.color = colors.bold.yellow;
      levelFmtData.str = "WARN";
      break;
    }
    case LogLevel.ERROR: {
      levelFmtData.color = colors.bold.red;
      levelFmtData.str = "ERROR";
      break;
    }
    case LogLevel.FATAL: {
      levelFmtData.color = colors.bold.red.underline;
      levelFmtData.str = "FATAL";
      break;
    }
    case LogLevel.DEBUG: {
      levelFmtData.color = colors.bold.blue;
      levelFmtData.str = "DEBUG";
      break;
    }
    case LogLevel.TRACE: {
      levelFmtData.color = colors.bold.gray;
      levelFmtData.str = "TRACE";
      break;
    }
  }

  console.log(colors.white(`[${timeStr}]`), levelFmtData.color(levelFmtData.str), colors.white(`(${pid}) ${appName} [${context}] : ${message}`));
}

export enum LogLevel {
  TRACE = 1,
  DEBUG = 2,
  FATAL = 3,
  ERROR = 4,
  WARN = 5,
  INFO = 6,
}

type LogHandler = (
  level: LogLevel,
  context: string,
  message: string,
  ...args: unknown[]
) => void;

interface LoggerState {
  appName: string;
  handlers: Array<LogHandler>;
}

const state: LoggerState = {
  appName: "",
  handlers: [consoleLogHandler],
};

export const setLoggerAppName = (appName: string) => {
  state.appName = appName;
};

export const setLogHandlers = (logHandlers: Array<LogHandler>) => {
  state.handlers = [...logHandlers];
};

export class Logger {
  constructor(private ctx: string) {}

  private handle(level: LogLevel, message: string, ...args: unknown[]) {
    state.handlers.forEach((h) => h(level, this.ctx, message, ...args));
  }

  info(message: string, ...args: unknown[]) {
    this.handle(LogLevel.INFO, message, ...args);
  }

  warn(message: string, ...args: unknown[]) {
    this.handle(LogLevel.WARN, message, ...args);
  }

  error(message: string, ...args: unknown[]) {
    this.handle(LogLevel.ERROR, message, ...args);
  }

  fatal(message: string, ...args: unknown[]) {
    this.handle(LogLevel.FATAL, message, ...args);
  }

  debug(message: string, ...args: unknown[]) {
    this.handle(LogLevel.DEBUG, message, ...args);
  }

  trace(message: string, ...args: unknown[]) {
    this.handle(LogLevel.TRACE, message, ...args);
  }
}
