import { registerContainer } from "./libs/di.ts";
import { UmmModule } from "./modules/umm.module.ts";
import { setLoggerAppName } from "./libs/logger.ts";

setLoggerAppName("UMM");
registerContainer(UmmModule);
