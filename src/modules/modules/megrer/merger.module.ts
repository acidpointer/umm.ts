import { Container } from "../../../libs/di.ts";
import { DefinitionsProvider } from "./providers/definitions.provider.ts";
import { MergerProvider } from "./providers/merger.provider.ts";
import { IntegrityProvider } from "./providers/integrity.provider.ts";

@Container({
  providers: [
    DefinitionsProvider,
    MergerProvider,
    IntegrityProvider
  ]
})
export class MergerModule {}