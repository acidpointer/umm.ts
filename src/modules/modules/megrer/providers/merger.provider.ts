// deno-lint-ignore-file no-empty
import * as fs from "std/fs/mod.ts";
import * as path from "std/path/mod.ts";
import { Injectable } from "../../../../libs/di.ts";
import { DefinitionsProvider } from "./definitions.provider.ts";
import { Logger } from "../../../../libs/logger.ts";
import { Mod } from "../types/definitions.types.ts";
import { MASTERMOD_PREFIX } from "../constants/merget.const.ts";


@Injectable()
export class MergerProvider {
  private readonly logger = new Logger("Merger");

  constructor(
    private dependencies: { definitionsProvider: DefinitionsProvider },
  ) {}

  async merge(useSymlinks = true) {
    const definitions = this.dependencies.definitionsProvider.definitions!;
    const workDirAbsPath = this.dependencies.definitionsProvider.workDir;
    const backupsAbsPath = path.join(
      workDirAbsPath,
      definitions.paths.backupsRelativePath,
    );
    const modsFolderAbsPath = path.join(
      workDirAbsPath,
      definitions.paths.modsRelativePath,
    );

    var filesChanged = 0;
    
    var processedModsCount = 0;

    var masterMod = "";

    const processMod = async (mod: Mod, baseAbsPath: string) => {
      if (typeof mod.enabled === 'boolean') {
        if (mod.enabled === false) {
          return;
        }
      }


      const modAbsPath = path.join(baseAbsPath, mod.relativePath);

      if (Array.isArray(mod.submods)) {
        masterMod = path.join(masterMod, this.clearMasterModName(mod.relativePath));
        for (const submod of mod.submods) {
          await processMod(submod, modAbsPath);
        }

        masterMod = "";
        return;
      }

      masterMod = this.clearMasterModName(masterMod);

      this.logger.info(`(${++processedModsCount}) ${masterMod ? `[${masterMod}] ` : ''}Enabled mod: ${mod.relativePath}`);

      for await (
        const entry of fs.walk(modAbsPath, {
          includeDirs: true,
          includeFiles: true,
          includeSymlinks: false,
        })
      ) {
        const entryRelPath = path.relative(modAbsPath, entry.path);

        if (!entryRelPath) continue;

        const baseAbsPath = path.join(workDirAbsPath, entryRelPath);

        if (entry.isDirectory) {
          await fs.ensureDir(baseAbsPath);
        }

        if (entry.isFile) {
          try {
            const baseFileLstat = await Deno.lstat(baseAbsPath);

            // Backup file
            if (baseFileLstat.isFile) {
              const backupFileAbsPath = path.join(
                backupsAbsPath,
                entryRelPath,
              );

              await fs.ensureDir(path.dirname(backupFileAbsPath));
              await fs.move(baseAbsPath, backupFileAbsPath);
            }

            if (baseFileLstat.isSymlink) {
              await Deno.remove(baseAbsPath);
            }
          } catch (_) {}

          try {
            if (useSymlinks) {
              await Deno.symlink(entry.path, baseAbsPath);
              filesChanged += 1;
              continue;
            }
            await Deno.copyFile(entry.path, baseAbsPath);
            filesChanged += 1;
          } catch (err) {
            this.logger.error(
              `Cant merge file: ${entry.path}. Error: ${err?.stack}`,
            );
          }
        }
      }
    };

    for (const mod of definitions.mods) {
      await processMod(mod, modsFolderAbsPath);
    }

    this.logger.info(`Total files changed: ${filesChanged}`);
    this.logger.info(`Total mods installed: ${processedModsCount}`);
  }

  private clearMasterModName(masterMod: string): string {
    if (masterMod.startsWith(MASTERMOD_PREFIX)) {
      masterMod = masterMod.replace(MASTERMOD_PREFIX, '');
    }

    return masterMod;
  }
}