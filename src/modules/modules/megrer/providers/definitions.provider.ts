import * as fs from "std/fs/mod.ts";
import * as path from "std/path/mod.ts";

import { Injectable } from "../../../../libs/di.ts";
import { Logger } from "../../../../libs/logger.ts";

import { Definitions } from "../types/definitions.types.ts";
import { Mod } from "../types/definitions.types.ts";
import { readAndParseYaml } from "../utils/yaml.utils.ts";
import { writeYaml } from "../utils/yaml.utils.ts";
import { sortMods } from "../utils/mod.utils.ts";
import { MASTERMOD_PREFIX } from "../constants/merget.const.ts";

const defaultDefinitions: Definitions = {
  paths: {
    modsRelativePath: "umm_mods",
    backupsRelativePath: "umm_backups",
    workspaceRelativePath: ".",
  },
  autoScan: false,
  mods: [],
};

const definitionsFileName = "definitions.yml";


@Injectable()
export class DefinitionsProvider {
  private readonly logger = new Logger("Definitions");

  #definitionsPath = "";
  #workDir = "";

  #definitions: Definitions | null = null;

  get definitions(): Definitions | null {
    return this.#definitions;
  }

  get workDir(): string {
    return this.#workDir;
  }

  async ensureDefinitions(workDir: string, createBlank = false) {
    await this.setWorkDirOrError(workDir);

    if (!(await fs.exists(this.#definitionsPath)) && createBlank) {
      this.#definitions = await this.createDefaultDefinitions();
      this.logger.info(`Blank definitions created: '${this.#definitionsPath}'`);
    }

    this.#definitions = await this.readDefinitions();
    this.logger.info(
      `Definitions '${this.#definitionsPath}' parsed successfull!`,
    );
  }

  async scan() {
    if (!this.#definitions) {
      return;
    }

    const modsRelPath = this.#definitions?.paths.modsRelativePath;
    const modsAbsPath = path.join(this.#workDir, modsRelPath);

    var totalMods = 0;

    const processMods = async (
      absPath: string,
    ): Promise<Array<Mod>> => {
      const mods: Array<Mod> = [];

      for await (const entry of Deno.readDir(absPath)) {
        const entryAbsPath = path.join(absPath, entry.name);

        if (entry.name.startsWith("?") || entry.name === "fomod") {
          continue;
        }

        if (entry.isDirectory) {
          const newMod: Mod = {
            enabled: true,
            relativePath: entry.name,
          };

          mods.push(newMod);

          if (entry.name.startsWith(MASTERMOD_PREFIX)) {
            newMod.submods = sortMods(await processMods(entryAbsPath));
            continue;
          }

          totalMods += 1;
        }
      }

      return mods;
    };

    this.#definitions.mods = sortMods(await processMods(modsAbsPath));

    await this.writeDefinitions(this.#definitions);
    this.logger.info(`Total mods amount: ${totalMods}`);
  }

  private async writeDefinitions(definitions: Definitions) {
    return writeYaml(
      definitions,
      this.#definitionsPath,
    );
  }

  private async setWorkDirOrError(workDir: string) {
    const lstat = await Deno.lstat(workDir);

    if (lstat.isDirectory) {
      workDir = await Deno.realPath(workDir);
      this.#definitionsPath = path.join(workDir, definitionsFileName);
      this.#workDir = workDir;
      return;
    }

    throw new Error(`Workdir: ${workDir} not valid!`);
  }

  private async createDefaultDefinitions(): Promise<Definitions> {
    await this.writeDefinitions(defaultDefinitions);

    return defaultDefinitions as unknown as Definitions;
  }

  private async readDefinitions(): Promise<Definitions> {
    const parsed = await readAndParseYaml(this.#definitionsPath);

    return parsed as Definitions;
  }
}
