import { Injectable } from "../../../../libs/di.ts";
import { DefinitionsProvider } from "./definitions.provider.ts";
//import { HashFile } from "../../../../libs/hash-file.ts";


@Injectable()
export class IntegrityProvider {
  #integrityPath = ""

  constructor(private deps: { definitionsProvider: DefinitionsProvider }) {}

  get integrityPath(): string {
    return this.#integrityPath;
  }

  async calculateIntegrity() {
    
  }
  
}