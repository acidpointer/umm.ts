import * as toml from "std/toml/mod.ts";

export const readAndParseToml = async <T>(filePath: string): Promise<T> => {
  const decoder = new TextDecoder("utf-8");

  const definitionsRawContent = await Deno.readFile(filePath);
  const definitionsStrContent = decoder.decode(definitionsRawContent);

  return toml.parse(definitionsStrContent) as T;
}

export const writeToml = async (
  content: unknown,
  path: string,
): Promise<void> => {
  const enc = new TextEncoder();

  const stringifiedDefinitions = toml.stringify(
    content as Record<string, unknown>,
    {
      keyAlignment: true
    }
  );

  return Deno.writeFile(
    path,
    enc.encode(stringifiedDefinitions),
  );
};
