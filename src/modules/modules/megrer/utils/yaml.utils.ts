import { stringify, parse } from "yaml";

export const readAndParseYaml = async (filePath: string): Promise<unknown> => {
  const decoder = new TextDecoder("utf-8");
  const definitionsRawContent = await Deno.readFile(filePath);
  const definitionsStrContent = decoder.decode(definitionsRawContent);

  return parse(definitionsStrContent, { version: 'next' });
};

export const writeYaml = async (
  content: unknown,
  path: string,
): Promise<void> => {
  const enc = new TextEncoder();
  const stringifiedDefinitions = stringify(content, { version: 'next' });

  return Deno.writeFile(
    path,
    enc.encode(stringifiedDefinitions),
  );
};
