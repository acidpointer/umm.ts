import * as ini from "std/ini/mod.ts";

export const readAndParseIni = async <T>(filePath: string): Promise<T> => {
  const decoder = new TextDecoder("utf-8");

  const definitionsRawContent = await Deno.readFile(filePath);
  const definitionsStrContent = decoder.decode(definitionsRawContent);

  return ini.parse(definitionsStrContent) as T;
}

export const writeIni = async (
  content: unknown,
  path: string,
): Promise<void> => {
  const enc = new TextEncoder();

  const stringifiedDefinitions = ini.stringify(
    content as Record<string, unknown>,
    {
        pretty: true
    }
  );

  return Deno.writeFile(
    path,
    enc.encode(stringifiedDefinitions),
  );
};
