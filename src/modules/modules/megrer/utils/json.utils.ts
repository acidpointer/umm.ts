export const readAndParseJson = async <T>(
  filePath: string,
): Promise<T> => {
  const decoder = new TextDecoder("utf-8");

  const definitionsRawContent = await Deno.readFile(filePath);

  const definitionsStrContent = decoder.decode(definitionsRawContent);

  return JSON.parse(definitionsStrContent);
};

export const readAndParseJsonSync = <T>(
  filePath: string,
): T => {
  const decoder = new TextDecoder("utf-8");

  const definitionsRawContent = Deno.readFileSync(filePath);

  const definitionsStrContent = decoder.decode(definitionsRawContent);

  return JSON.parse(definitionsStrContent);
};

export const writeJson = async (
  content: unknown,
  path: string,
): Promise<void> => {
  const enc = new TextEncoder();

  const stringifiedDefinitions = JSON.stringify(
    content,
    null,
    2,
  );

  return Deno.writeFile(
    path,
    enc.encode(stringifiedDefinitions),
  );
};
