// deno-lint-ignore-file no-inner-declarations
import { Mod } from "../types/definitions.types.ts";

const extractNumbersFromString = (str: string): number[] => {
  const numbers: Array<number> = [];
  var currentNumber = "";

  for (const char of str) {
    if (/\d/.test(char)) {
      currentNumber += char;
    } else if (currentNumber !== "") {
      numbers.push(Number(currentNumber));
      currentNumber = "";
    }
  }

  if (currentNumber !== "") {
    numbers.push(Number(currentNumber));
  }

  return numbers;
};

const compareMods = (mod1: Mod, mod2: Mod): number => {
  const nums1 = extractNumbersFromString(mod1.relativePath);
  const nums2 = extractNumbersFromString(mod2.relativePath);

  // If one of the mods does not contain numbers, we will place it lower in the sorting process
  if (nums1.length === 0 && nums2.length > 0) {
    return 1;
  } else if (nums1.length > 0 && nums2.length === 0) {
    return -1;
  } else if (nums1.length === 0 && nums2.length === 0) {
    // Если обе строки не содержат чисел, сравниваем их напрямую
    // Дополнительно учитываем их длину
    if (mod1.relativePath.length !== mod2.relativePath.length) {
      return mod1.relativePath.length - mod2.relativePath.length;
    }
    return mod1.relativePath.localeCompare(mod2.relativePath);
  }

  // If both modes do not contain numbers or both do, we compare their numbers
  const minLength = Math.min(nums1.length, nums2.length);
  for (var i = 0; i < minLength; i++) {
    const diff = nums1[i] - nums2[i];
    if (diff !== 0) {
      return diff;
    }
  }

  //return nums1.length - nums2.length;

  // Если строки содержат одинаковые числа, сравниваем их по длине
  if (mod1.relativePath.length !== mod2.relativePath.length) {
    return mod1.relativePath.length - mod2.relativePath.length;
  }

  // Если строки содержат одинаковые числа и длины, сравниваем их напрямую
  return mod1.relativePath.localeCompare(mod2.relativePath);
};

export const sortMods = (mods: Array<Mod>): Array<Mod> => {
  mods.sort(compareMods);
  mods.forEach((mod) => {
    if (mod.submods) {
      mod.submods = sortMods(mod.submods);
    }
  });

  return mods;
};
