export interface Mod {
  enabled: boolean;
  relativePath: string;
  submods?: Array<Mod>;
}

export interface Definitions {
  paths: {
    modsRelativePath: string;
    backupsRelativePath: string;
    workspaceRelativePath: string;
  };

  autoScan: boolean;
  mods: Array<Mod>;
}
