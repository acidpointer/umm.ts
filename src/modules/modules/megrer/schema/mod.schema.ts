import { z } from "zod";

const baseModSchema = z.object({
  enabled: z.boolean(),
  relativePath: z.string(),
});

type Mod = z.infer<typeof baseModSchema> & {
  submods?: Array<Mod>;
}

export const modSchema: z.ZodType<Mod> = baseModSchema.extend({
  submods: z.lazy(() => baseModSchema.array().optional())
});

export default modSchema;