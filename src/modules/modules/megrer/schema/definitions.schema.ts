import { z } from "zod";
import { modSchema } from "./mod.schema.ts";

export const definitionsSchema = z.object({
  paths: z.object({
    modsRelativePath: z.string(),
    backupsRelativePath: z.string(),
    workspaceRelativePath: z.string(),
  }),

  autoScan: z.boolean().default(false),

  mods: z.array(modSchema),
});

export default definitionsSchema;
