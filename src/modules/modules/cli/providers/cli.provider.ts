import { InitializableProvider, Injectable } from "../../../../libs/di.ts";
import { Logger } from "../../../../libs/logger.ts";
import { Command, ValidationError } from "cliffy_command";
import { DefinitionsProvider } from "../../megrer/providers/definitions.provider.ts";
import { MergerProvider } from "../../megrer/providers/merger.provider.ts";

@Injectable()
export class CliProvider implements InitializableProvider {
  private readonly logger = new Logger("CLI");

  constructor(
    private deps: {
      definitionsProvider: DefinitionsProvider;
      mergerProvider: MergerProvider;
    },
  ) {}

  async onInit(): Promise<void> {
    const errorHandler = (error: Error, cmd: Command) => {
      if (error instanceof ValidationError) {
        cmd.showHelp();
        return;
      }
      this.logger.fatal(`Error: ${error?.stack}`);
      Deno.exit(error instanceof ValidationError ? error.exitCode : 1);
    };

    const mergeComand = new Command()
      .name("merge")
      .description(
        "Merge content of each mod directory with work directory. Depends on rules in definitions.yml",
      )
      .option("-s, --symlinks", "Do not use symlinks. Default copying instead")
      .action(({ symlinks }) => this.deps.mergerProvider.merge(symlinks));

    const scanCommand = new Command()
      .name("scan")
      .description(
        "Scan mods directory for mods and/or submods. WILL REWRITE definitions.yml!",
      )
      .action(() => this.deps.definitionsProvider.scan());

    await new Command()
      .name("umm")
      .description("Universal mod manager.")
      .usage("[options] [action]")
      .version("0.0.1")
      .option(
        "-i, --init",
        "Initialize. If no definitions found, new blank will be created.",
        {
          global: true
        }
      )
      .option(
        "-w, --workdir <workdir:string>",
        "Work directory (directory with game)",
        {
          global: true
        }
      )
      .globalAction(async ({ workdir, init }) => {
        if (!workdir?.length) {
          throw new ValidationError(
            "Please, select the work directory with '-w' option. Work directory - directory with game, or where 'definitions.yml' exist.",
          );
        }
        await this.deps.definitionsProvider.ensureDefinitions(workdir, init);
      })
      .command("scan", scanCommand)
      .command("merge", mergeComand)
      .error(errorHandler)
      .parse();
  }
}
