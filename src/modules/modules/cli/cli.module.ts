import { Container } from "../../../libs/di.ts";
import { CliProvider } from "./providers/cli.provider.ts";
import { MergerModule } from "../megrer/merger.module.ts";

@Container({
  providers: [CliProvider],
  containers: [MergerModule]
})
export class CliModule {}