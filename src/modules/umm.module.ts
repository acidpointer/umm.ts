import { Container } from "../libs/di.ts";
import { MergerModule } from "./modules/megrer/merger.module.ts";
import { CliModule } from "./modules/cli/cli.module.ts";

@Container({
  containers: [MergerModule, CliModule],
})
export class UmmModule {}
